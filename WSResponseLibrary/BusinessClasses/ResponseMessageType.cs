using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Web.Services.Protocols;
using System.ServiceModel;
using System.Threading;

using ESBCurrent;
using www.cjis.iowa.gov.CJISExchangeESBFault;

namespace www.cjis.iowa.gov.WSResponse.one.zero
{
    public partial class ResponseMessageType
    {
        private Guid mSelfID = Guid.NewGuid();
        private bool mblnIsOutGoing = false;
        private string mstrOutURL = string.Empty;
        private string mstrSentStatus = string.Empty;
        private int mintRetries = 0;

        private string mstrOriginalMessageDirection = Globals.c_gstrMessageDirectionIn;
        private string mstrRawPayload = null;

        private string XMLData
        {
            get
            {
                return clsXMLData.SerializeObject(this);
            }
        }

        [XmlIgnoreAttribute]
        public string RawPayload
        {
            get
            {
                return mstrRawPayload;
            }
            set
            {
                mstrRawPayload = value;
            }
        }

        [XmlIgnoreAttribute]
        public bool IsOutGoing
        {
            get
            {
                return mblnIsOutGoing;
            }
            set
            {
                mblnIsOutGoing = value;
            }
        }

        internal int Retries
        {
            get
            {
                return this.mintRetries;
            }
            set
            {
                mintRetries = value;
            }
        }

        [XmlIgnoreAttribute]
        public string SentStatus
        {
            get
            {
                return mstrSentStatus;
            }
            set
            {
                mstrSentStatus = value;
            }
        }

        [XmlIgnoreAttribute]
        public string OriginalMessageDirection
        {
            get
            {
                return mstrOriginalMessageDirection;
            }
            set
            {
                mstrOriginalMessageDirection = value;
            }
        }

        internal string OutURL
        {
            get
            {
                return mstrOutURL;
            }
            set
            {
                mstrOutURL = value;
            }
        }

        [XmlIgnoreAttribute]
        public string ErrorHeader
        {
            get
            {
                string lstrErrorHeader = string.Empty;

                if (this.MessageId == null)
                {
                    lstrErrorHeader += "MessageID: Unknown \n";
                }
                else
                {
                    lstrErrorHeader += "MessageID: " + this.MessageId + "\n";
                }

                return lstrErrorHeader;
            }
        }

        #region "Business Methods"

        public string SchemaValidation(bool vUseRawPayload = false)
        {
            // Validate against main Lex schema
            string lstrSchemaLocation = Globals.SchemaLocation;
            string lstrXMLValidation = string.Empty;

            if (Globals.SchemaValidationOn)
            {
                TextReader lobjTextReader = null;

                if (vUseRawPayload)
                {
                    lobjTextReader = new StringReader(this.RawPayload);
                }
                else
                {
                    lobjTextReader = new StringReader(this.XMLData);
                }

                lstrXMLValidation = XMLValidator.Validate(lobjTextReader, lstrSchemaLocation);

                if (lstrXMLValidation != string.Empty)
                {
                    if (vUseRawPayload)
                    {
                        lstrXMLValidation = "(RawPayload) Schema validation Errors against main schema are as below: \n\n" + lstrXMLValidation + "\n\n";
                    }
                    else
                    {
                        lstrXMLValidation = "(XMLData) Schema validation Errors against main schema are as below: \n\n" + lstrXMLValidation + "\n\n";
                    }
                }
            }

            return lstrXMLValidation;
        }

        public string BusinessValidation()
        {
            string lstrErrorMsg = string.Empty;

            return lstrErrorMsg;
        }

        #endregion

        #region "Factory Methods"

        public static ResponseMessageType GetInstance(SqlDataReader dr)
        {
            ResponseMessageType lobjInstance = new ResponseMessageType();

            try
            {
                lobjInstance.doDataLoad(dr);
            }
            catch (Exception ex)
            {
                string lstrErrorMsg = "Error reading staging database.\n" + ex.Message + "\n" + ex.StackTrace.ToString();
                ErrorItem.WriteEventLog(lstrErrorMsg, EventLogEntryType.Error, ErrorItemType.System, EmailModeType.Default);

                lobjInstance = null;
            }

            return lobjInstance;
        }

        public static ResponseMessageType GetInstanceFromXML(string vXML)
        {
            ResponseMessageType lobjInstance = new ResponseMessageType();

            try
            {
                lobjInstance = (ResponseMessageType)clsXMLData.DeSerializeObject(vXML, lobjInstance.GetType());
            }
            catch (Exception ex)
            {
                //ErrorItem.WriteEventLog(ex.Message, EventLogEntryType.Error, ErrorItemType.System, EmailModeType.Never); 
                throw ex;
            }

            lobjInstance.RawPayload = vXML;

            return lobjInstance;
        }

        public static ResponseMessageType GetInstanceFromFile(string vFileName)
        {
            ResponseMessageType lobjInstance = new ResponseMessageType();

            try
            {
                lobjInstance.fetchFromFile(vFileName);
            }
            catch (Exception ex)
            {
                lobjInstance = null;

                string lstrErrorMsg = "Error reading xml file.\n" + ex.ToString() + "\n" + ex.StackTrace.ToString();
                ErrorItem.WriteEventLog(lstrErrorMsg, EventLogEntryType.Error, ErrorItemType.System, EmailModeType.Default);
            }

            return lobjInstance;
        }

        #endregion

        #region "Data Access"

        public ResponseMessageType save(string vMode, bool vUseRawPayload = false)
        {
            ResponseMessageType lobjReturn = null;
            string lstrBusinessValidation = string.Empty;
            string lstrXMLValidation = string.Empty;

            try
            {
                if (lstrBusinessValidation == string.Empty)
                {
                    // Schema Validation
                    if (vMode == Globals.c_gstrActionCodeTypeAdd)
                    {
                        lstrXMLValidation = this.SchemaValidation(vUseRawPayload);
                    }

                    if (lstrXMLValidation == string.Empty)
                    {
                        // save to file
                        if (Globals.WriteToFile)
                        { // write out xml instances to files
                            string lstrFileName = this.MessageId + "-" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xml";
                            this.saveToFile(lstrFileName, vUseRawPayload);
                        }

                        try
                        {
                            using (SqlConnection cn = new SqlConnection(Globals.StageDB))
                            {
                                cn.Open();
                                using (SqlCommand cm = cn.CreateCommand())
                                {
                                    cm.CommandType = CommandType.StoredProcedure;
                                    cm.CommandTimeout = Globals.SQLCommandTimeOutInterval;

                                    switch (vMode)
                                    {
                                        case Globals.c_gstrActionCodeTypeAdd:
                                            cm.CommandText = "uspAllInsertMessageStatus";
                                            DoInsert(cm, vUseRawPayload);
                                            break;

                                        case Globals.c_gstrActionCodeTypeModify:
                                            cm.CommandText = "uspAllUpdateMessageStatus";
                                            DoUpdate(cm);
                                            break;

                                        default:
                                            break;
                                    }
                                } // end of using 

                                if (cn != null && cn.State == ConnectionState.Open)
                                {
                                    cn.Close();
                                }

                                // prepare successful return
                                lobjReturn = new ResponseMessageType();

                                lobjReturn.MessageId = this.MessageId;
                                lobjReturn.Description = this.Description;
                                lobjReturn.TransactionStatus = Globals.c_gstrTransactionStatusSuccess;
                                lobjReturn.ErrorCode = "0";
                                lobjReturn.ErrorMsg = "Async response was successfully saved to staging database.";
                                lobjReturn.AgencyId = string.Empty;
                                lobjReturn.KeyData = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");
                                lobjReturn.FromAgencyId = Globals.LocalAgencyID;
                            } // end of using
                        }
                        catch (Exception ex)
                        {
                            // prepare failure return
                            lobjReturn = new ResponseMessageType();

                            lobjReturn.MessageId = this.MessageId;
                            lobjReturn.Description = this.Description;
                            lobjReturn.TransactionStatus = Globals.c_gstrTransactionStatusFailure;
                            lobjReturn.ErrorCode = "999";
                            lobjReturn.ErrorMsg = "Async response failed in saving to staging database: " + ex.Message;
                            lobjReturn.KeyData = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");
                            lobjReturn.FromAgencyId = Globals.LocalAgencyID;
                            lobjReturn.AgencyId = string.Empty;

                            // throw error
                            throw ex;
                        }
                    } // end of successful schema validation 
                    else
                    {
                        lobjReturn = new ResponseMessageType();

                        lobjReturn.MessageId = this.MessageId;
                        lobjReturn.Description = this.Description;
                        lobjReturn.TransactionStatus = Globals.c_gstrTransactionStatusFailure;
                        lobjReturn.ErrorCode = "888";
                        lobjReturn.ErrorMsg = "Message failed in schema validation: " + lstrXMLValidation;
                        lobjReturn.KeyData = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");
                        lobjReturn.FromAgencyId = Globals.LocalAgencyID;
                        lobjReturn.AgencyId = string.Empty;
                    }
                } // end of successful business validation
                else // did not pass business validation
                {
                    lobjReturn = new ResponseMessageType();

                    lobjReturn.MessageId = this.MessageId;
                    lobjReturn.Description = this.Description;
                    lobjReturn.TransactionStatus = Globals.c_gstrTransactionStatusFailure;
                    lobjReturn.ErrorCode = "111";
                    lobjReturn.ErrorMsg = "Message failed in business validation: " + lstrBusinessValidation;
                    lobjReturn.KeyData = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");
                    lobjReturn.FromAgencyId = Globals.LocalAgencyID;
                    lobjReturn.AgencyId = string.Empty;
                }

                // Log error
                if (lobjReturn != null)
                {
                    if (Globals.DebugMode)
                    {
                        if (lobjReturn.TransactionStatus == Globals.c_gstrTransactionStatusSuccess)
                        {
                            string sb = string.Empty;
                            sb += "Success\n";
                            sb += this.ErrorHeader + "\n";
                            sb += lobjReturn.ErrorMsg;

                            ErrorItem.WriteEventLog(sb, EventLogEntryType.Information, ErrorItemType.User, EmailModeType.Default);
                        }
                    }
                }
            } // end of outer Try
            catch (Exception ex)
            {
                if (lobjReturn == null)
                {
                    string lstrErrorMsg = "Unexpected save error: " + ex.Message + "\n";

                    lobjReturn = new ResponseMessageType();

                    lobjReturn.MessageId = this.MessageId;
                    lobjReturn.Description = this.Description;
                    lobjReturn.TransactionStatus = Globals.c_gstrTransactionStatusFailure;
                    lobjReturn.ErrorCode = "666";
                    lobjReturn.ErrorMsg = lstrErrorMsg;
                    lobjReturn.KeyData = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");
                    lobjReturn.FromAgencyId = Globals.LocalAgencyID;
                    lobjReturn.AgencyId = string.Empty;
                }
            }

            return lobjReturn;
        }

        private void DoInsert(SqlCommand cm, bool vUseRawPayload)
        {
            if (this.MessageId == null)
            {
                cm.Parameters.AddWithValue(Globals.c_gstrMessageIDParam, DBNull.Value);
            }
            else
            {
                cm.Parameters.AddWithValue(Globals.c_gstrMessageIDParam, this.MessageId);
            }

            if (this.Description == null)
            {
                cm.Parameters.AddWithValue(Globals.c_gstrDescriptionParam, DBNull.Value);
            }
            else
            {
                cm.Parameters.AddWithValue(Globals.c_gstrDescriptionParam, this.Description);
            }

            if (this.TransactionStatus == null)
            {
                cm.Parameters.AddWithValue(Globals.c_gstrTransactionStatusParam, DBNull.Value);
            }
            else
            {
                cm.Parameters.AddWithValue(Globals.c_gstrTransactionStatusParam, this.TransactionStatus);
            }

            if (this.ErrorCode == null)
            {
                cm.Parameters.AddWithValue(Globals.c_gstrErrorCodeParam, DBNull.Value);
            }
            else
            {
                cm.Parameters.AddWithValue(Globals.c_gstrErrorCodeParam, this.ErrorCode);
            }

            if (this.ErrorMsg == null)
            {
                cm.Parameters.AddWithValue(Globals.c_gstrErrorMsgParam, DBNull.Value);
            }
            else
            {
                cm.Parameters.AddWithValue(Globals.c_gstrErrorMsgParam, this.ErrorMsg);
            }

            if (this.KeyData == null)
            {
                cm.Parameters.AddWithValue(Globals.c_gstrKeyDataParam, DBNull.Value);
            }
            else
            {
                cm.Parameters.AddWithValue(Globals.c_gstrKeyDataParam, this.KeyData);
            }

            if (this.FromAgencyId == null)
            {
                cm.Parameters.AddWithValue(Globals.c_gstrFromAgencyIDParam, DBNull.Value);
            }
            else
            {
                cm.Parameters.AddWithValue(Globals.c_gstrFromAgencyIDParam, this.FromAgencyId);
            }

            if (this.AgencyId == null)
            {
                cm.Parameters.AddWithValue(Globals.c_gstrToAgencyIDParam, DBNull.Value);
            }
            else
            {
                cm.Parameters.AddWithValue(Globals.c_gstrToAgencyIDParam, this.AgencyId);
            }

            cm.Parameters.AddWithValue(Globals.c_gstrIsOutGoingParam, this.IsOutGoing);

            if (this.SentStatus == string.Empty)
            {
                cm.Parameters.AddWithValue(Globals.c_gstrSentStatusParam, DBNull.Value);
            }
            else
            {
                cm.Parameters.AddWithValue(Globals.c_gstrSentStatusParam, this.SentStatus);
            }

            if (this.OriginalMessageDirection == string.Empty)
            {
                cm.Parameters.AddWithValue(Globals.c_gstrOriginalMessageDirectionParam, DBNull.Value);
            }
            else
            {
                cm.Parameters.AddWithValue(Globals.c_gstrOriginalMessageDirectionParam, this.OriginalMessageDirection);
            }

            if (vUseRawPayload)
            {
                if (this.RawPayload == null)
                {
                    cm.Parameters.AddWithValue(Globals.c_gstrXMLDataParam, DBNull.Value);
                }
                else
                {
                    cm.Parameters.AddWithValue(Globals.c_gstrXMLDataParam, this.RawPayload);
                }
            }
            else
            {
                if (this.XMLData == null)
                {
                    cm.Parameters.AddWithValue(Globals.c_gstrXMLDataParam, DBNull.Value);
                }
                else
                {
                    cm.Parameters.AddWithValue(Globals.c_gstrXMLDataParam, this.XMLData);
                }
            }

            cm.ExecuteNonQuery();
        }

        private void DoUpdate(SqlCommand cm)
        {
            cm.Parameters.AddWithValue(Globals.c_gstrMessageStatusIDParam, this.mSelfID);

            if (this.SentStatus == string.Empty)
            {
                cm.Parameters.AddWithValue(Globals.c_gstrSentStatusParam, DBNull.Value);
            }
            else
            {
                cm.Parameters.AddWithValue(Globals.c_gstrSentStatusParam, this.SentStatus);
            }

            cm.ExecuteNonQuery();
        }

        public void saveToFile(string vFileName, bool vUseRawPayload = false)
        {
            string fcnname = "saveToFile";
            TextWriter tr = null;

            try
            {
                if (vUseRawPayload)
                {
                    tr = new StreamWriter(vFileName);
                    tr.Write(this.RawPayload);
                }
                else
                {
                    //setup namespaces if needed
                    //XmlSerializerNamespaces ns = new XmlSerializerNamespaces();

                    //ns.Add("vt", "http://www.cjis.iowa.gov/document/1.0");	// VictimTransfer.xsd
                    //ns.Add("ia", "http://www.cjis.iowa.gov/extension/1.0");// ia_extension.xsd
                    //ns.Add("jx", "http://www.it.ojp.gov/jxdm/3.0.3");	// jxdm.xsd

                    XmlSerializer serializer = new XmlSerializer(this.GetType());
                    tr = new StreamWriter(vFileName);
                    serializer.Serialize(tr, this);
                }

                tr.Close();
            }
            catch (Exception ex)
            {
                string sb = string.Empty;
                sb = sb + "className: " + this.GetType() + "\n";
                sb = sb + "fcnname: " + fcnname + "\n";
                sb = sb + "Error\n" + ex.ToString();

                ErrorItem.WriteEventLog(sb, EventLogEntryType.Error, ErrorItemType.System, EmailModeType.Default);
            }
        }

        public bool submit()
        {
            string fcnname = "submit";
            string lstrErrorMsg = string.Empty;
            bool lblnReturn = false;
            bool lblnRetry = false;
            ErrorItemType lErrorItemType = ErrorItemType.System;

            bool lblnWriteToFile = Globals.WriteToFile;
            string lstrSchemaLocation = Globals.SchemaLocation;

            try
            {
                lstrErrorMsg = this.BusinessValidation();
                lErrorItemType = ErrorItemType.User;

                if (lstrErrorMsg == string.Empty)
                {   // pass business validation, let's do schema validation 
                    lstrErrorMsg += this.SchemaValidation();

                    if (lstrErrorMsg == string.Empty)
                    { // pass schema validation, ready to send out  

                        if (lblnWriteToFile)
                        { // write out xml instances to files
                            try
                            {
                                if (this != null)
                                {
                                    string lstrFileName = this.MessageId + "-" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xml";
                                    this.saveToFile(lstrFileName, false);
                                }
                            }
                            catch (Exception ex)
                            {
                                string sb = string.Empty;
                                sb = sb + "className: " + this.GetType() + "\n";
                                sb = sb + "fcnname: " + fcnname + "\n";
                                sb = sb + "WriteToFile Error\n" + ex.ToString();

                                ErrorItem.WriteEventLog(sb, EventLogEntryType.Warning, ErrorItemType.System, EmailModeType.Default);
                            }
                        }

                        ResponseMessageType lobjReturnResponse = null;

                        try
                        {
                            // calls the web service
                            int lintImmediateDoubleSendCounter = 0;
                            int lintImmediateDoubleSendMaxCounter = 0;

                            if (Globals.ImmediateDoubleSend)
                            {
                                lintImmediateDoubleSendMaxCounter = 1;
                            }

                            while (lintImmediateDoubleSendCounter <= lintImmediateDoubleSendMaxCounter)
                            {
                                IowaAsyncPortTypeClient proxy = new IowaAsyncPortTypeClient("customEndPoint");
                                EndpointIdentity lobjEndpointIdentity = null;

                                if (proxy.Endpoint != null)
                                {
                                    if (proxy.Endpoint.Address != null)
                                    {
                                        if (proxy.Endpoint.Address.Identity != null)
                                        {
                                            lobjEndpointIdentity = proxy.Endpoint.Address.Identity;
                                        }
                                    }
                                }

                                proxy.Close();
                                proxy = null;

                                if (lobjEndpointIdentity == null)
                                {
                                    proxy = new IowaAsyncPortTypeClient("customEndPoint", new EndpointAddress(new Uri(this.OutURL)));
                                }
                                else
                                {
                                    proxy = new IowaAsyncPortTypeClient("customEndPoint", new EndpointAddress(new Uri(this.OutURL), lobjEndpointIdentity));
                                }

                                if (lintImmediateDoubleSendCounter < lintImmediateDoubleSendMaxCounter)
                                { // before the last round
                                    try
                                    {
                                        lobjReturnResponse = proxy.SubmitAsync(this);
                                        // when success, skip the loop
                                        lintImmediateDoubleSendCounter = lintImmediateDoubleSendMaxCounter + 1;
                                    }
                                    catch (Exception ex)
                                    {
                                        lintImmediateDoubleSendCounter += 1;
                                        string lstrTempErrorMsg = this.ErrorHeader + "\n";
                                        lstrTempErrorMsg += "First time error (Immediate retry is ready): " + ex.Message + ".\n";
                                        if (ex.InnerException != null)
                                        {
                                            lstrTempErrorMsg += ex.InnerException.Message + "\n";
                                        }

                                        lstrTempErrorMsg += ex.StackTrace + "\n";
                                        // Only writes warning event, do not send email before last immediate retry
                                        ErrorItem.WriteEventLog(lstrTempErrorMsg, EventLogEntryType.Warning, ErrorItemType.System, EmailModeType.Never);
                                    }

                                    Thread.Sleep(Globals.DelayInBetweenSend);
                                }
                                else
                                {
                                    lobjReturnResponse = proxy.SubmitAsync(this);
                                    // when success, skip the loop
                                    lintImmediateDoubleSendCounter = lintImmediateDoubleSendMaxCounter + 1;
                                }
                            }
                        } // end of Try calling web service
                        catch (FaultException<FaultInfo> ex)
                        {
                            if (ex.Code.IsReceiverFault)
                            {
                                lblnRetry = true;
                            }

                            if (ex.Detail == null)
                            {
                                lstrErrorMsg += "(FaultException<FaultInfo>)Submit to ESB failed: " + "\n" + "Message [" + this.MessageId + "]\n " + ex.Message + "\n";
                            }
                            else
                            {
                                lstrErrorMsg += "(FaultException<FaultInfo>)Submit to ESB failed: " + "\n" + "Message [" + this.MessageId + "]\n " + ex.Message + "\n" + ex.Detail.FaultDetail;
                            }
                            lErrorItemType = ErrorItemType.System;
                        }
                        catch (FaultException<ExceptionDetail> ex)
                        {
                            if (ex.Code.IsReceiverFault)
                            {
                                lblnRetry = true;
                            }

                            if (ex.Detail == null)
                            {
                                lstrErrorMsg += "(FaultException<ExceptionDetail>)Submit to ESB failed: " + "\n" + "Message [" + this.MessageId + "]\n " + ex.Message + "\n";
                            }
                            else
                            {
                                lstrErrorMsg += "(FaultException<ExceptionDetail>)Submit to ESB failed: " + "\n" + "Message [" + this.MessageId + "]\n " + ex.Message + "\n" + ex.Detail.Message;
                            }
                            lErrorItemType = ErrorItemType.System;
                        }
                        catch (FaultException ex)
                        {
                            if (ex.Code.IsReceiverFault)
                            {
                                lblnRetry = true;
                            }

                            lstrErrorMsg += "(FaultException)Submit to ESB failed: " + "\n" + "Message [" + this.MessageId + "]\n " + ex.Message + "\n";
                            if (ex.InnerException != null)
                            {
                                lstrErrorMsg += ex.InnerException.Message + "\n";
                            }

                            lErrorItemType = ErrorItemType.System;
                        }
                        catch (Exception ex)
                        {
                            lblnRetry = true;
                            lstrErrorMsg += "(Exception)Submit to ESB failed: " + "\n" + "Message [" + this.MessageId + "]\n" + ex.Message + "\n";
                            lErrorItemType = ErrorItemType.System;
                        }

                        if (lobjReturnResponse != null)
                        {
                            if (lobjReturnResponse.TransactionStatus.ToLower().StartsWith("s"))
                            { // successful response
                                lblnReturn = true;
                            }
                            else
                            {
                                lstrErrorMsg += "ESB Sync Error. \n" + lobjReturnResponse.ErrorMsg + "\n";
                                lErrorItemType = ErrorItemType.System;
                            }
                        }
                    } // end of pass schema validation
                    else
                    {
                        lblnReturn = false;
                        lErrorItemType = ErrorItemType.User;
                    }
                } // end of pass business validation
                else
                {
                    lblnReturn = false;
                    lErrorItemType = ErrorItemType.User;
                }
            }
            catch (Exception ex)
            {
                lblnRetry = true;

                lstrErrorMsg += "generic (Exception): " + ex.Message + "\n";

                if (ex.InnerException != null)
                {
                    lstrErrorMsg += ex.InnerException.Message + "\n";
                }

                lstrErrorMsg += ex.StackTrace + "\n";

                lErrorItemType = ErrorItemType.System;
            }

            if (lblnReturn)
            {
                string sb = this.ErrorHeader + "Submitted successfully. \n";
                ErrorItem.WriteEventLog(sb, EventLogEntryType.Information, ErrorItemType.Success, EmailModeType.Default);

                this.SentStatus = Globals.EndMessageStatus;
                this.save(Globals.c_gstrActionCodeTypeModify);
            }
            else
            {
                string sb = this.ErrorHeader + "Submitted with error. \n";

                if (lblnRetry && (this.Retries < Globals.MaxRetries))
                {
                    sb += "Retry was scheduled. \n" + lstrErrorMsg;
                }
                else
                {
                    sb += "Permanent failure happened. \n" + lstrErrorMsg;
                }

                ErrorItem.WriteEventLog(sb, EventLogEntryType.Error, lErrorItemType, EmailModeType.Default);

                if (lblnRetry)
                {
                    this.SentStatus = Globals.c_gstrSentStatusAASR;
                    this.save(Globals.c_gstrActionCodeTypeModify);
                }
                else
                {
                    this.SentStatus = Globals.c_gstrSentStatusAASE;
                    this.save(Globals.c_gstrActionCodeTypeModify);
                }
            }

            return lblnReturn;
        }

        private void fetchFromFile(string vFileName)
        {
            /*            //load data for itself
                        string lstrExchangeName = ConfigurationManager.AppSettings["ExchangeName"].ToUpper();
                        mResponseMessage.MessageId = vFileName + DateTime.Now.ToString("yyyyMMddHHmmss");

                        //load data for children
                        string lstrMessageID = this.ResponseMessage.MessageId;

                        mBusinessObject = VictimTransferType.GetInstanceFromFile(vFileName);
              */
        }

        internal void doDataLoad(SqlDataReader dr)
        {
            //load data for itself

            // MessageID
            if (dr.GetValue(dr.GetOrdinal(Globals.c_gstrMessageID)) != DBNull.Value)
            {
                this.MessageId = dr.GetString(dr.GetOrdinal(Globals.c_gstrMessageID)).Trim();
            }
            else
            {
                this.MessageId = string.Empty;
            }

            // MessageStatusID
            if (dr.GetValue(dr.GetOrdinal(Globals.c_gstrMessageStatusID)) != DBNull.Value)
            {
                this.mSelfID = dr.GetGuid(dr.GetOrdinal(Globals.c_gstrMessageStatusID));
            }
            else
            {
                this.mSelfID = Guid.Empty;
            }

            // Description
            if (dr.GetValue(dr.GetOrdinal(Globals.c_gstrDescription)) != DBNull.Value)
            {
                this.Description = dr.GetString(dr.GetOrdinal(Globals.c_gstrDescription)).Trim();
            }
            else
            {
                this.Description = string.Empty;
            }

            // TransactionStatus
            if (dr.GetValue(dr.GetOrdinal(Globals.c_gstrTransactionStatus)) != DBNull.Value)
            {
                this.TransactionStatus = dr.GetString(dr.GetOrdinal(Globals.c_gstrTransactionStatus)).Trim();
            }
            else
            {
                this.TransactionStatus = string.Empty;
            }

            // ErrorCode
            if (dr.GetValue(dr.GetOrdinal(Globals.c_gstrErrorCode)) != DBNull.Value)
            {
                this.ErrorCode = dr.GetString(dr.GetOrdinal(Globals.c_gstrErrorCode)).Trim();
            }
            else
            {
                this.ErrorCode = string.Empty;
            }

            // ErrorMsg
            if (dr.GetValue(dr.GetOrdinal(Globals.c_gstrErrorMsg)) != DBNull.Value)
            {
                this.ErrorMsg = dr.GetString(dr.GetOrdinal(Globals.c_gstrErrorMsg)).Trim();
            }
            else
            {
                this.ErrorMsg = string.Empty;
            }

            // KeyData
            if (dr.GetValue(dr.GetOrdinal(Globals.c_gstrKeyData)) != DBNull.Value)
            {
                this.KeyData = dr.GetString(dr.GetOrdinal(Globals.c_gstrKeyData)).Trim();
            }
            else
            {
                this.KeyData = string.Empty;
            }

            // FromAgencyID
            if (dr.GetValue(dr.GetOrdinal(Globals.c_gstrFromAgencyID)) != DBNull.Value)
            {
                this.FromAgencyId = dr.GetString(dr.GetOrdinal(Globals.c_gstrFromAgencyID)).Trim();
            }
            else
            {
                this.FromAgencyId = null;
            }

            // ToAgencyID
            if (dr.GetValue(dr.GetOrdinal(Globals.c_gstrToAgencyID)) != DBNull.Value)
            {
                this.AgencyId = dr.GetString(dr.GetOrdinal(Globals.c_gstrToAgencyID)).Trim();
            }
            else
            {
                this.AgencyId = string.Empty;
            }

            // OutURL
            if (dr.GetValue(dr.GetOrdinal(Globals.c_gstrOutURL)) != DBNull.Value)
            {
                this.OutURL = dr.GetString(dr.GetOrdinal(Globals.c_gstrOutURL)).Trim();
            }
            else
            {
                this.OutURL = string.Empty;
            }

            //load data for children
        }

        #endregion
    }

    public class ResponseMessageTypeCol : System.Collections.Generic.List<ResponseMessageType>
    {
        private int mintProcessSuccessfullyCount = 0;

        #region "Factory Methods"

        public static ResponseMessageTypeCol GetInstance()
        {
            int i = int.MaxValue;

            return ResponseMessageTypeCol.GetInstance(i);
        }

        public static ResponseMessageTypeCol GetInstance(int vMaxRequests)
        {
            ResponseMessageTypeCol lcolInstance = new ResponseMessageTypeCol();

            lcolInstance.fetch(vMaxRequests);

            return lcolInstance;
        }

        #endregion

        #region "Business properties"

        public int ProcessSuccessfullyCount
        {
            get
            {
                return mintProcessSuccessfullyCount;
            }
        }

        #endregion

        #region "Process Messages"

        public void submit()
        {
            string fcnname = "submit";

            foreach (ResponseMessageType lobjResponseMessage in this)
            {
                try
                {
                    if (lobjResponseMessage.submit())
                    {
                        mintProcessSuccessfullyCount = mintProcessSuccessfullyCount + 1;
                    }
                }

                catch (Exception ex)
                {
                    string sb = string.Empty;
                    sb = sb + "className: " + this.GetType().ToString() + "\n";
                    sb = sb + "fcnname: " + fcnname + "\n";
                    sb = sb + "Error\n" + ex.ToString();

                    ErrorItem.WriteEventLog(sb, EventLogEntryType.Error, ErrorItemType.System, EmailModeType.Default);
                }
            }
        }

        #endregion

        #region "Data Access"

        private bool fetch(int vMaxRequests)
        {
            string fcnname = "fetch";
            int i = 0;

            bool lblnSendOnlySendAsync = Globals.SendOnlySendAsync;
            bool lblnReadFromFile = Globals.ReadFromFile;
            string lstrReadFileName = Globals.ReadFileName;

            if (!lblnReadFromFile)
            {   // read from tblMessageStatus
                using (SqlConnection cn = new SqlConnection(Globals.StageDB))
                {
                    cn.Open();
                    using (SqlCommand cm = cn.CreateCommand())
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cm.CommandTimeout = Globals.SQLCommandTimeOutInterval;

                        cm.CommandText = "uspAllGetAsyncMessages";

                        cm.Parameters.AddWithValue(Globals.c_gstrExchangeNameParam, Globals.ExchangeName);
                        cm.Parameters.AddWithValue(Globals.c_gstrSendOnlySendAsyncParam, lblnSendOnlySendAsync);

                        using (SqlDataReader dr = cm.ExecuteReader())
                        {
                            while (dr.Read() && (i < vMaxRequests))
                            {
                                i++;
                                try
                                {
                                    doDataLoad(dr);
                                }
                                catch (Exception ex)
                                {
                                    string sb = string.Empty;
                                    sb = sb + "className: " + this.GetType().ToString() + "\n";
                                    sb = sb + "fcnname: " + fcnname + "\n";
                                    sb = sb + "Error\n" + ex.ToString() + "\n" + ex.StackTrace.ToString();

                                    ErrorItem.WriteEventLog(sb, EventLogEntryType.Error, ErrorItemType.System, EmailModeType.Default);
                                }
                            }
                        }
                    }
                }
                return true;
            }
            else
            { // read from test file
                try
                {
                    ResponseMessageType lObject;

                    lObject = ResponseMessageType.GetInstanceFromFile(lstrReadFileName);

                    if (lObject != null)
                    {
                        this.Add(lObject);
                    }
                }
                catch (Exception ex)
                {
                    string sb = string.Empty;
                    sb = sb + "className: " + this.GetType().ToString() + "\n";
                    sb = sb + "fcnname: " + fcnname + "\n";
                    sb = sb + "Error\n" + ex.ToString() + "\n" + ex.StackTrace.ToString();

                    ErrorItem.WriteEventLog(sb, EventLogEntryType.Error, ErrorItemType.System, EmailModeType.Default);
                }
                return true;
            }
        }

        internal void doDataLoad(SqlDataReader dr)
        {
            ResponseMessageType lObject;

            lObject = ResponseMessageType.GetInstance(dr);

            if (lObject != null)
            {
                this.Add(lObject);
            }
        }

        #endregion
    }
}

/*
' DotNetNuke -  http://www.dotnetnuke.com
' Copyright (c) 2002-2005
' by Shaun Walker ( sales@perpetualmotion.ca ) of Perpetual Motion Interactive Systems Inc. ( http://www.perpetualmotion.ca )
'
' Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
' documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
' the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
' to permit persons to whom the Software is furnished to do so, subject to the following conditions:
'
' The above copyright notice and this permission notice shall be included in all copies or substantial portions 
' of the Software.
'
' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
' TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
' THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
' CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
' DEALINGS IN THE SOFTWARE.
*/
using System;
using System.Reflection;

    /*********************************************************************
    '
    ' Null Class
    '
    ' Class for dealing with the translation of database null values. 
    '
    '*********************************************************************/

public class Null
{
    // define application encoded null values 
    public static short NullShort
    {
        get
        {
            return -1;
        }
    }

    public static int NullInteger
    {
        get
        {
            return -1;
        }
    }

    public static Single NullSingle
    {
        get
        {
            return Single.MinValue;
        }
    }

    public static Double NullDouble
    {
        get
        {
            return Double.MinValue;
        }
    }

    public static Decimal NullDecimal
    {
        get
        {
            return Decimal.MinValue;
        }
    }

    public static DateTime NullDateTime
    {
        get
        {
            return DateTime.MinValue;
        }
    }

    public static string NullString
    {
        get
        {
            return "";
        }
    }

    public static bool NullBool
    {
        get
        {
            return false;
        }
    }

    public static Guid NullGuid
    {
        get
        {
            return Guid.Empty;
        }
    }

    // sets a field to an application encoded null value ( used in BLL layer )
    public static object SetNull(object objValue, object objField)
    {
        object lSetNull;

        if (objValue == DBNull .Value )
        {
            if (objField is short)
            {
                lSetNull = NullShort;
            }
            else
            {
                if (objField is int)
                {
                    lSetNull = NullInteger;
                }
                else
                {
                    if (objField is Single)
                    {
                        lSetNull = NullSingle;
                    }
                    else
                    {
                        if (objField is Double)
                        {

                            lSetNull = NullDouble;
                        }
                        else
                        {
                            if (objField is Decimal)
                            {
                                lSetNull = NullDecimal;
                            }
                            else
                            {
                                if (objField is DateTime)
                                {
                                    lSetNull = NullDateTime;
                                }
                                else
                                {
                                    if (objField is String)
                                    {
                                        lSetNull = NullString;
                                    }
                                    else
                                    {
                                        if (objField is bool)
                                        {
                                            lSetNull = NullBool;
                                        }
                                        else
                                        {
                                            if (objField is Guid)
                                            {
                                                lSetNull = NullGuid;
                                            }
                                            else
                                            {
                                                lSetNull = null;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        else
        {
            lSetNull = objValue;
        }

        return lSetNull;
    }

    // sets a field to an application encoded null value ( used in BLL layer )
    public static object SetNull(PropertyInfo objPropertyInfo)
    {
        object lSetNull;

        switch (objPropertyInfo.PropertyType.ToString())
        {
            case "System.Int16":
                lSetNull = NullShort;
                break;
            case "System.Int32":
                lSetNull = NullInteger;
                break;
            case "System.Int64":
                lSetNull = NullInteger;
                break;
            case "System.Single":
                lSetNull = NullSingle;
                break;
            case "System.Double":
                lSetNull = NullDouble;
                break;
            case "System.Decimal":
                lSetNull = NullDecimal;
                break;
            case "System.DateTime":
                lSetNull = NullDateTime;
                break;
            case "System.String":
                lSetNull = NullString;
                break;
            case "System.Char":
                lSetNull = NullString;
                break;
            case "System.Boolean":
                lSetNull = NullBool;
                break;
            case "System.Guid":
                lSetNull = NullGuid;
                break;
            default:
                // Enumerations default to the first entry
                Type pType = objPropertyInfo.PropertyType;
                if (pType.BaseType == typeof(System.Enum))
                {
                    Array objEnumValues = System.Enum.GetValues(pType);
                    Array.Sort(objEnumValues);
                    lSetNull = System.Enum.ToObject(pType, objEnumValues.GetValue(0));
                }
                else // complex object
                {
                    lSetNull = null;
                }
                break;
        }

        return lSetNull;
    }

    public static object GetNull(object objField)
    {
        return Null.GetNull(objField, DBNull.Value);
    }

    // convert an application encoded null value to a database null value ( used in DAL )
    public static object GetNull(object objField, object objDBNull)
    {
        object lGetNull = objField;

        if (objField == null)
        {
            lGetNull = objDBNull;
        }
        else
        {
            if (objField is short)
            {
                if (Convert.ToInt16(objField) == NullShort)
                {
                    lGetNull = objDBNull;
                }
            }
            else
            {
                if (objField is int)
                {
                    if (Convert.ToInt32(objField) == NullInteger)
                    {
                        lGetNull = objDBNull;
                    }
                }
                else
                {
                    if (objField is Single)
                    {
                        if (Convert.ToSingle(objField) == NullSingle)
                        {
                            lGetNull = objDBNull;
                        }
                    }
                    else
                    {
                        if (objField is Double)
                        {
                            if (Convert.ToDouble(objField) == NullDouble)
                            {
                                lGetNull = objDBNull;
                            }
                        }
                        else
                        {
                            if (objField is Decimal)
                            {
                                if (Convert.ToDecimal(objField) == NullDecimal)
                                {
                                    lGetNull = objDBNull;
                                }
                            }
                            else
                            {
                                if (objField is DateTime)
                                {
                                    // compare the Date part of the DateTime with the DatePart of the NullDate ( this avoids subtle time differences )
                                    if (Convert.ToDateTime(objField).Date == NullDateTime.Date)
                                    {
                                        lGetNull = objDBNull;
                                    }
                                }
                                else
                                {
                                    if (objField is string)
                                    {
                                        if (objField == null)
                                        {
                                            lGetNull = objDBNull;
                                        }
                                        else
                                        {
                                            if (objField.ToString() == NullString)
                                            {
                                                lGetNull = objDBNull;
                                            }

                                        }
                                    }
                                    else
                                    {
                                        if (objField is bool)
                                        {
                                            if (Convert.ToBoolean(objField) == NullBool)
                                            {
                                                lGetNull = objDBNull;

                                            }
                                        }
                                        else
                                        {
                                            if (objField is Guid)
                                            {
                                                if ((Guid)objField == NullGuid)
                                                {
                                                    lGetNull = objDBNull;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return lGetNull;
    }


    // checks if a field contains an application encoded null value
    public static bool IsNull(object objField)
    {
        bool lblnIsNull;

        if (objField != null)
        {
            if (objField is int)
            {
                lblnIsNull = objField.Equals(NullInteger);
            }
            else
            {
                if (objField is Single)
                {
                    lblnIsNull = objField.Equals(NullSingle);
                }
                else
                {
                    if (objField is Double)
                    {
                        lblnIsNull = objField.Equals(NullDouble);
                    }
                    else
                    {
                        if (objField is Decimal)
                        {
                            lblnIsNull = objField.Equals(NullDecimal);
                        }
                        else
                        {
                            if (objField is DateTime)
                            {
                                DateTime objDate = (DateTime )objField;
                                lblnIsNull = objDate.Date.Equals(NullDateTime.Date);
                            }
                            else
                            {
                                if (objField is string)
                                {
                                    lblnIsNull = objField.Equals(NullString);
                                }
                                else
                                {
                                    if (objField is bool)
                                    {
                                        lblnIsNull = objField.Equals(NullBool);
                                    }
                                    else
                                    {
                                        if (objField is Guid)
                                        {
                                            lblnIsNull = objField.Equals(NullGuid);
                                        }
                                        else
                                        {
                                            lblnIsNull = false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        else
        {
            lblnIsNull = true;
        }

        return lblnIsNull;
    }
}
using System;
using System.Xml;
using System.Web.Services;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Web.Services.Protocols;
using System.Xml.Serialization;
using System.Threading;
using System.Net;
using System.Net.Mail;
using System.ServiceModel;
using System.ServiceModel.Channels;

using www.cjis.iowa.gov.CJISExchangeESBFault;

namespace www.cjis.iowa.gov.WSResponse.one.zero
{
    public class ErrorItem
    {
        private static int SystemErrorCounter;

        private static void WriteEventLog(String vLogName, String vSourceName, String vMessage, EventLogEntryType vType)
        {
            try
            {
                string lstrMinimumNotificationLevel = Globals.MinimumNotificationLevel;
                int lintMinimumLevel = Convert.ToInt32(lstrMinimumNotificationLevel);

                int lintRequestLevel = 99;

                switch (vType)
                {
                    case EventLogEntryType.Error:
                        lintRequestLevel = 1;
                        break;
                    case EventLogEntryType.Warning:
                        lintRequestLevel = 2;
                        break;
                    case EventLogEntryType.Information:
                        lintRequestLevel = 3;
                        break;
                    case EventLogEntryType.FailureAudit:
                        lintRequestLevel = 4;
                        break;
                    case EventLogEntryType.SuccessAudit:
                        lintRequestLevel = 5;
                        break;
                    default:
                        lintRequestLevel = 99;
                        break;
                }

                if (lintRequestLevel <= lintMinimumLevel)
                {
                    //Create EventSource if not exist
                    if (!EventLog.SourceExists(vSourceName))
                    {
                        EventLog.CreateEventSource(vSourceName, vLogName);
                    }

                    EventLog lEventLog = new EventLog();
                    lEventLog.Source = vSourceName;

                    lEventLog.WriteEntry(vMessage, vType);
                }
            }
            catch (Exception ex)
            {
                Debug.Print(ex.Message);
            }
        }

        public static void WriteEventLog(String vMessage, EventLogEntryType vType, ErrorItemType vErrorItemType, EmailModeType vEmailMode)
        {
            try
            {
                // add datetime stamp
                vMessage += "\n\n" + "This eventlog occurred @ " + DateTime.Now.ToString() + "\n";

                // event log
                try
                {
                    ErrorItem.WriteEventLog(Globals.EventLog, Globals.EventSource, vMessage, vType);
                }
                catch (Exception ex)
                {
                    Debug.Print(ex.Message);
                }

                // email
                bool lblnSendEmailOnDataError = Globals.SendEmailOnDataError;
                bool lblnSendEmailOnSystemError = Globals.SendEmailOnSystemError;

                if ((lblnSendEmailOnDataError || lblnSendEmailOnSystemError || (vEmailMode == EmailModeType.Always)) && (vEmailMode != EmailModeType.Never))
                {
                    try
                    {
                        ErrorItem.SendEmail(Globals.ProductName, vMessage, vType, vErrorItemType, vEmailMode);
                    }
                    catch (Exception ex)
                    {
                        Debug.Print(ex.Message);
                    }
                }
            }

            catch (Exception ex)
            {
                Debug.Print(ex.Message);
            }
        }

        public static void ThrowSoapException(string vMessage, XmlQualifiedName vCode, string vDetail)
        {
            // write to event log first
            string lstrError = vMessage + "\n" + vDetail;
            WriteEventLog(lstrError, EventLogEntryType.Error, ErrorItemType.System, EmailModeType.Default);

            bool lblnUseSoapFault12 = false;

            if (OperationContext.Current.IncomingMessageVersion == MessageVersion.Soap12)
            {
                lblnUseSoapFault12 = true;
            }

            if (OperationContext.Current.IncomingMessageVersion == MessageVersion.Soap12WSAddressing10)
            {
                lblnUseSoapFault12 = true;
            }

            if (OperationContext.Current.IncomingMessageVersion == MessageVersion.Soap12WSAddressingAugust2004)
            {
                lblnUseSoapFault12 = true;
            }

            if (lblnUseSoapFault12)
            {
                if (vCode == SoapException.ServerFaultCode)
                {
                    vCode = Soap12FaultCodes.ReceiverFaultCode;
                }
                if (vCode == SoapException.ClientFaultCode)
                {
                    vCode = Soap12FaultCodes.SenderFaultCode;
                }
            }
            else
            {
                if (vCode == Soap12FaultCodes.ReceiverFaultCode)
                {
                    vCode = SoapException.ServerFaultCode;
                }
                if (vCode == Soap12FaultCodes.SenderFaultCode)
                {
                    vCode = SoapException.ClientFaultCode;
                }
            }

            // FaultInfo a = new FaultInfo();
            FaultInfo lobjFaultInfo = new FaultInfo(vCode.Name, vMessage, vDetail);
            FaultReason lobjFaultReason = new FaultReason(vMessage);
            FaultCode lobjFaultCode = new FaultCode(vCode.Name);

            throw new FaultException<FaultInfo>(lobjFaultInfo, lobjFaultReason, lobjFaultCode);
        }

        private static void SendEmail(string vSubject, string vMsg, EventLogEntryType vType, ErrorItemType vErrorItemType, EmailModeType vEmailMode)
        {
            try
            {
                string lstrEmailFrom = Globals.EmailFrom;

                bool lblnSendEmailOnDataError = Globals.SendEmailOnDataError;
                string lstrDataErrorEmailTo = Globals.DataErrorEmailTo;

                bool lblnSendEmailOnSystemError = Globals.SendEmailOnSystemError;
                string lstrSystemErrorEmailTo = Globals.SystemErrorEmailTo;
                int lintSystemErrorEmailMax = Globals.SystemErrorEmailMax;

                string lstrSMTPHost = Globals.SMTPHost;
                int lintSMTPPort = Globals.SMTPPort;
                string lstrSMTPUser = Globals.SMTPUser;
                string lstrSMTPPass = Globals.SMTPPass;
                string lstrSMTPDomain = Globals.SMTPDomain;
                bool lblnSMTPRequiresCredential = Globals.SMTPRequiresCredential;

                int lintUserMinimumLevel = Globals.MinimumUserEmailNotificationLevel;
                int lintSystemMinimumLevel = Globals.MinimumSystemEmailNotificationLevel;

                string lstrEmailAdditionalSubject = Globals.EmailAdditionalSubject;

                vSubject = vSubject + "(" + vErrorItemType.ToString() + ")" + lstrEmailAdditionalSubject;

                bool lblnSendSyncEmail = Globals.SendSyncEmail;
                bool lblnLogEmailActivity = Globals.LogEmailActivity;
                bool lblnTrulySendEmail = false;

                int lintRequestLevel = 99;

                switch (vType)
                {
                    case EventLogEntryType.Error:
                        lintRequestLevel = 1;
                        break;
                    case EventLogEntryType.Warning:
                        lintRequestLevel = 2;
                        break;
                    case EventLogEntryType.Information:
                        lintRequestLevel = 3;
                        break;
                    case EventLogEntryType.FailureAudit:
                        lintRequestLevel = 4;
                        break;
                    case EventLogEntryType.SuccessAudit:
                        lintRequestLevel = 5;
                        break;
                    default:
                        lintRequestLevel = 99;
                        break;
                }

                if (vErrorItemType == ErrorItemType.Success)
                {   // when success, reset SystemErrorCounter
                    SystemErrorCounter = 0;
                }

                SmtpClient client = new SmtpClient();

                client.Host = lstrSMTPHost;
                client.Port = lintSMTPPort;
                client.UseDefaultCredentials = false;

                if (lblnSMTPRequiresCredential)
                {
                    client.Credentials = new NetworkCredential(lstrSMTPUser, lstrSMTPPass, lstrSMTPDomain);
                }

                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.EnableSsl = Globals.EnableSsl;

                string lstrUserState = string.Empty;

                if ((vErrorItemType == ErrorItemType.User) || (vErrorItemType == ErrorItemType.Success))
                {
                    if (((lintRequestLevel <= lintUserMinimumLevel) || (vEmailMode == EmailModeType.Always)) && (vEmailMode != EmailModeType.Never))
                    {
                        if (lblnSendEmailOnDataError)
                        {
                            if (lblnSendSyncEmail)
                            {
                                client.Send(lstrEmailFrom, lstrDataErrorEmailTo, vSubject, vMsg);
                            }
                            else
                            {
                                client.SendAsync(lstrEmailFrom, lstrDataErrorEmailTo, vSubject, vMsg, lstrUserState);
                            }

                            lblnTrulySendEmail = true;
                        }
                    }
                }

                if ((vErrorItemType == ErrorItemType.System) || (vErrorItemType == ErrorItemType.Success))
                {
                    if (((lintRequestLevel <= lintSystemMinimumLevel) || (vEmailMode == EmailModeType.Always)) && (vEmailMode != EmailModeType.Never))
                    {
                        SystemErrorCounter += 1;
                        if (lblnSendEmailOnSystemError)
                        {
                            if (SystemErrorCounter <= lintSystemErrorEmailMax)
                            {
                                if (lblnSendSyncEmail)
                                {
                                    client.Send(lstrEmailFrom, lstrSystemErrorEmailTo, vSubject, vMsg);
                                }
                                else
                                {
                                    client.SendAsync(lstrEmailFrom, lstrSystemErrorEmailTo, vSubject, vMsg, lstrUserState);
                                }

                                lblnTrulySendEmail = true;
                            }
                        }
                    }
                }

                Thread.Sleep(1000);

                // Log email activity
                if (lblnLogEmailActivity && lblnTrulySendEmail)
                {
                    string sb = string.Empty;

                    sb = sb + "SMTP Server IP: " + client.Host + "\n";
                    sb = sb + "SMTP Port: " + client.Port + "\n";

                    sb = sb + "MAIL FROM: " + lstrEmailFrom + "\n";
                    if (vErrorItemType == ErrorItemType.User)
                    {
                        sb = sb + "RCPT TO: " + lstrDataErrorEmailTo + "\n";
                    }
                    else
                    {
                        sb = sb + "RCPT TO: " + lstrSystemErrorEmailTo + "\n";
                    }
                    sb = sb + "Subject: " + vSubject + "\n";
                    sb = sb + "DATA: \n" + vMsg;
                    ErrorItem.WriteEventLog(Globals.EventLog, Globals.EventSource, sb, EventLogEntryType.Information);
                }
            }

            catch (Exception ex)
            {
                ErrorItem.WriteEventLog(Globals.EventLog, Globals.EventSource, ex.Message, EventLogEntryType.Error);
            }
        }
    }
}


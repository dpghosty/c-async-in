using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Xml;
using System.Xml.Schema;
using System.Text;

public class XMLValidator
{
    // Validation Error Count
    static int ErrorsCount = 0;

    // Validation Error Message
    static string ErrorMessage = string.Empty;

    private static void ValidationCallBack(object sender, ValidationEventArgs e)
    {
        ErrorMessage = ErrorMessage + e.Message + "\n";
        ErrorsCount++;
    }

    public static string Validate(MemoryStream vXMLDocument, string vSchemaUri)
    {
        string lstrReturn = string.Empty;

        try
        {
            ErrorMessage = string.Empty;
            ErrorsCount = 0;

            // Create the XmlSchemaSet class.
            XmlSchemaSet sc = new XmlSchemaSet();

            // Add the schema to the collection.
            sc.Add(null, vSchemaUri);

            // Set the validation settings.
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.ValidationType = ValidationType.Schema;
            settings.Schemas = sc;
            settings.ValidationEventHandler += new ValidationEventHandler(ValidationCallBack);

            // Create the XmlReader object.
            vXMLDocument.Position = 0;
            XmlReader reader = XmlReader.Create(vXMLDocument, settings);

            int i = 0;

            // Parse the file. 
            using (reader)
            {
                while (reader.Read())
                {
                    i = i + 1;
                }
            }

            //while (reader.Read()) ;

            reader.Close();

            // Raise exception, if XML validation fails
            if (ErrorsCount > 0)
            {
                string lstrErrorMessage = ErrorMessage;

                ErrorMessage = string.Empty;
                ErrorsCount = 0;

                throw new Exception(lstrErrorMessage);
            }

            // XML Validation succeeded
            lstrReturn = string.Empty;
        }
        catch (Exception error)
        {
            // XML Validation failed
            lstrReturn = "XML validation failed." + "\n" + error.Message;
        }

        return lstrReturn;
    }

    public static string Validate(TextReader vXMLDocument, string vSchemaUri)
    {
        string lstrReturn = string.Empty;
        StreamReader lobjStreamReader;
        StringReader lobjStringReader;

        if (vXMLDocument.GetType() == typeof(StreamReader))
        {
            lobjStreamReader = (StreamReader)vXMLDocument;
            lobjStreamReader.BaseStream.Position = 0;
            vXMLDocument = lobjStreamReader;
        }

        if (vXMLDocument.GetType() == typeof(StringReader))
        {
            lobjStringReader = (StringReader)vXMLDocument;
            vXMLDocument = lobjStringReader;
        }

        try
        {
            ErrorMessage = string.Empty;
            ErrorsCount = 0;

            // Create the XmlSchemaSet class.
            XmlSchemaSet sc = new XmlSchemaSet();

            // Add the schema to the collection.
            sc.Add(null, vSchemaUri);

            // Set the validation settings.
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.ValidationType = ValidationType.Schema;
            settings.Schemas = sc;
            settings.ValidationEventHandler += new ValidationEventHandler(ValidationCallBack);

            // Create the XmlReader object.
            XmlReader reader = XmlReader.Create(vXMLDocument, settings);

            int i = 0;

            // Parse the file. 
            using (reader)
            {
                while (reader.Read())
                {
                    i = i + 1;
                }
            }

            reader.Close();

            // Raise exception, if XML validation fails
            if (ErrorsCount > 0)
            {
                string lstrErrorMessage = ErrorMessage;

                ErrorMessage = string.Empty;
                ErrorsCount = 0;

                throw new Exception(lstrErrorMessage);
            }

            // XML Validation succeeded
            lstrReturn = string.Empty;
        }
        catch (Exception ex)
        {
            // XML Validation failed
            lstrReturn = "XML validation failed." + "\n" + ex.Message;
        }

        return lstrReturn;
    }

    private static string UTF8ByteArrayToString(Byte[] characters)
    {
        UTF8Encoding encoding = new UTF8Encoding();
        string constructedString = encoding.GetString(characters);
        return (constructedString);
    }
}

using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace www.cjis.iowa.gov.WSResponse.one.zero
{
    public class Globals
    {
        // ActionCode type
        public const string c_gstrActionCodeTypeAdd = "A"; 
        public const string c_gstrActionCodeTypeModify = "M";
        public const string c_gstrActionCodeTypeDelete = "D";

        // TransactionStatus
        public const string c_gstrTransactionStatusSuccess = "S";
        public const string c_gstrTransactionStatusFailure = "E";

        internal const string c_gstrMessageID = "MessageID";
        internal const string c_gstrMessageStatusID = "MessageStatusID";
        internal const string c_gstrDescription = "Description";
        internal const string c_gstrTransactionStatus = "TransactionStatus";
        internal const string c_gstrErrorCode = "ErrorCode";
        internal const string c_gstrErrorMsg = "ErrorMsg";
        internal const string c_gstrKeyData = "KeyData";
        internal const string c_gstrFromAgencyID = "FromAgencyID"; 
        internal const string c_gstrToAgencyID = "ToAgencyID";
        internal const string c_gstrExchangeName = "ExchangeName";
        internal const string c_gstrOutURL = "OutURL";

        public const string c_gstrMessageDirectionIn = "In";
        public const string c_gstrMessageDirectionOut = "Out";

        internal const string c_gstrMessageIDParam = "@MessageID";
        internal const string c_gstrDescriptionParam = "@Description";
        internal const string c_gstrTransactionStatusParam = "@TransactionStatus";
        internal const string c_gstrErrorCodeParam = "@ErrorCode";
        internal const string c_gstrErrorMsgParam = "@ErrorMsg";
        internal const string c_gstrKeyDataParam = "@KeyData";
        internal const string c_gstrFromAgencyIDParam = "@FromAgencyID"; 
        internal const string c_gstrToAgencyIDParam = "@ToAgencyID";
        internal const string c_gstrMessageStatusParam = "@MessageStatus";
        internal const string c_gstrExchangeNameParam = "@ExchangeName";
        //internal const string c_gstrIsSyncResponseParam = "@IsSyncResponse";
        internal const string c_gstrIsOutGoingParam = "@IsOutGoing";
        public const string c_gstrXMLDataParam = "@XMLData";

        internal const string c_gstrMessageStatusIDParam = "@MessageStatusID";
        internal const string c_gstrSentStatusParam = "@SentStatus";
        internal const string c_gstrSendOnlySendAsyncParam = "@SendOnlySendAsync";
        internal const string c_gstrOriginalMessageDirectionParam = "@OriginalMessageDirection";

        /* 
         * tblMessage.MessageStatus value list
         *  For incoming message:
         *      Received With Error                 RWE
         *      Received and Ready to Process       RRP
         *      
         *      Processed With Error                PWE
         *      Processed With Sucess               PWS
         *      Process retry                       PWR -- not actual code in table
         *      
         *      tblMessageStatus.SentStatus
         *          Async ready to sent out             ARTS     
         * 
         *          Async Sent Successfully             AASS
         *          Async Sent with Error               AASE
         *          Async Sent Retry                    AASR -- not actual code in table

         *  For outgoing message:
         *      Message Ready to be Sent            MRS
         *      
         *      Message Sent with Success           MSS
         *      Message Sent with Error             MSE
         *      Message Sent Retry                  MSR -- not actual code in table
         *      
         *      tblMessageStatus.SentStatus     
         *          Async Received with Success         AARS
         *          Async Received with Error           AARE
         *      
         *          Async Processed with Success        AAPS
         *          Async Processed with Error          AAPE
         *          Async Process Retry                 AAPR -- not actual code in table
         */

        public const string c_gstrMessageStatusRWE = "RWE";
        public const string c_gstrMessageStatusRRP = "RRP";
        public const string c_gstrMessageStatusPWE = "PWE";
        public const string c_gstrMessageStatusPWS = "PWS";
        public const string c_gstrMessageStatusPWR = "PWR";

        public const string c_gstrSentStatusARTS = "ARTS";

        public const string c_gstrSentStatusAASS = "AASS";
        public const string c_gstrSentStatusAASE = "AASE";
        public const string c_gstrSentStatusAASR = "AASR";

        public const string c_gstrMessageStatusMRS = "MRS";
        public const string c_gstrMessageStatusMSS = "MSS";
        public const string c_gstrMessageStatusMSE = "MSE";
        public const string c_gstrMessageStatusMSR = "MSR";

        public const string c_gstrSentStatusAARS = "AARS";
        public const string c_gstrSentStatusAARE = "AARE";
        public const string c_gstrSentStatusAAPS = "AAPS";
        public const string c_gstrSentStatusAAPE = "AAPE";
        public const string c_gstrSentStatusAAPR = "AAPR";

        #region "AppSettings"

        // event log 
        public static string EventLog
        {
            get
            {
                return ConfigurationManager.AppSettings["EventLog"];
            }
        }

        public static string EventSource
        {
            get
            {
                return ConfigurationManager.AppSettings["EventSource"];
            }
        }

        public static string ProLawDB
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["ProLawDB"].ConnectionString;
            }
        }

        public static string StageDB
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["StageDB"].ConnectionString;
            }
        }

        public static string ExchangeName
        {
            get
            {
                return ConfigurationManager.AppSettings["ExchangeName"];
            }
        }

        public static string ProductName
        {
            get
            {
                return ConfigurationManager.AppSettings["ProductName"].ToUpper();
            }
        }

        public static int SQLCommandTimeOutInterval
        {
            get
            {
                return Convert.ToInt16(ConfigurationManager.AppSettings["SQLCommandTimeOutInterval"]);
            }
        }

        public static int MaxRequests
        {
            get
            {
                return Convert.ToInt16(ConfigurationManager.AppSettings["MaxRequests"]);
            }
        }

        public static int MaxRetries
        {
            get
            {
                return Convert.ToInt16(ConfigurationManager.AppSettings["MaxRetries"]);
            }
        }

        public static int RetryInterval
        {
            get
            {
                return Convert.ToInt16(ConfigurationManager.AppSettings["RetryInterval"]);
            }
        }

        public static int DelayInBetweenSend
        {
            get
            {
                return Convert.ToInt32(ConfigurationManager.AppSettings["DelayInBetweenSend"]);
            }
        }

        public static bool ImmediateDoubleSend
        {
            get
            {
                return Convert.ToBoolean(ConfigurationManager.AppSettings["ImmediateDoubleSend"]);
            }
        }

        public static int DaysToPurgeMessage
        {
            get
            {
                int lintDaysToPurgeMessage = Convert.ToInt16(ConfigurationManager.AppSettings["DaysToPurgeMessage"]);

                if (lintDaysToPurgeMessage < 90)
                {
                    lintDaysToPurgeMessage = 90;
                }

                return lintDaysToPurgeMessage;
            }
        }

        public static bool SendOnlySendAsync
        {
            get
            {
                return Convert.ToBoolean(ConfigurationManager.AppSettings["SendOnlySendAsync"]);
            }
        }

        public static bool ReadFromFile
        {
            get
            {
                return Convert.ToBoolean(ConfigurationManager.AppSettings["ReadFromFile"]);
            }
        }

        public static string ReadFileName
        {
            get
            {
                return ConfigurationManager.AppSettings["ReadFileName"].ToUpper();

            }
        }

        public static bool WriteToFile
        {
            get
            {
                return Convert.ToBoolean(ConfigurationManager.AppSettings["WriteToFile"]);

            }
        }

        public static string MinimumNotificationLevel
        {
            get
            {
                return ConfigurationManager.AppSettings["MinimumNotificationLevel"];
            }
        }

        public static string SMTPHost
        {
            get
            {
                return ConfigurationManager.AppSettings["SMTPHost"];
            }
        }

        public static int SMTPPort
        {
            get
            {
                return Convert.ToInt32(ConfigurationManager.AppSettings["SMTPPort"]);
            }
        }

        public static bool SMTPRequiresCredential
        {
            get
            {
                return Convert.ToBoolean(ConfigurationManager.AppSettings["SMTPRequiresCredential"]);
            }
        }

        public static string SMTPUser
        {
            get
            {
                return ConfigurationManager.AppSettings["SMTPUser"];
            }
        }

        public static string SMTPPass
        {
            get
            {
                return ConfigurationManager.AppSettings["SMTPPass"];
            }
        }

        public static string SMTPDomain
        {
            get
            {
                return ConfigurationManager.AppSettings["SMTPDomain"];
            }
        }

        public static bool EnableSsl
        {
            get
            {
                return Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]);
            }
        }

        public static string EmailFrom
        {
            get
            {
                return ConfigurationManager.AppSettings["EmailFrom"];
            }
        }

        public static bool SendEmailOnDataError
        {
            get
            {
                return Convert.ToBoolean(ConfigurationManager.AppSettings["SendEmailOnDataError"]);
            }
        }

        public static int MinimumUserEmailNotificationLevel
        {
            get
            {
                return Convert.ToInt32(ConfigurationManager.AppSettings["MinimumUserEmailNotificationLevel"]);
            }
        }

        public static string DataErrorEmailTo
        {
            get
            {
                return ConfigurationManager.AppSettings["DataErrorEmailTo"];
            }
        }

        public static bool SendEmailOnSystemError
        {
            get
            {
                return Convert.ToBoolean(ConfigurationManager.AppSettings["SendEmailOnSystemError"]);
            }
        }

        public static int MinimumSystemEmailNotificationLevel
        {
            get
            {
                return Convert.ToInt32(ConfigurationManager.AppSettings["MinimumSystemEmailNotificationLevel"]);
            }
        }

        public static string SystemErrorEmailTo
        {
            get
            {
                return ConfigurationManager.AppSettings["SystemErrorEmailTo"];
            }
        }

        public static int SystemErrorEmailMax
        {
            get
            {
                return Convert.ToInt32(ConfigurationManager.AppSettings["SystemErrorEmailMax"]);
            }
        }

        public static bool LogEmailActivity
        {
            get
            {
                return Convert.ToBoolean(ConfigurationManager.AppSettings["LogEmailActivity"]);
            }
        }

        public static bool SendSyncEmail
        {
            get
            {
                return Convert.ToBoolean(ConfigurationManager.AppSettings["SendSyncEmail"]);
            }
        }

        public static string EmailAdditionalSubject
        {
            get
            {
                return ConfigurationManager.AppSettings["EmailAdditionalSubject"];
            }
        }

        public static string LocalAgencyID
        {
            get
            {
                return ConfigurationManager.AppSettings["LocalAgencyID"];
            }
        }

        public static bool IsProcessingAgency
        {
            get
            {
                return Convert.ToBoolean(ConfigurationManager.AppSettings["IsProcessingAgency"]);
            }
        }

        public static string BeginMessageStatus
        {
            get
            {
                return ConfigurationManager.AppSettings["BeginMessageStatus"];
            }
        }

        public static string EndMessageStatus
        {
            get
            {
                return ConfigurationManager.AppSettings["EndMessageStatus"];
            }
        }

        public static double TimerInterval
        {
            get
            {
                return Convert.ToDouble(ConfigurationManager.AppSettings["TimerInterval"]);
            }
        }

        public static bool DebugMode
        {
            get
            {
                return Convert.ToBoolean(ConfigurationManager.AppSettings["DebugMode"]);
            }
        }

        public static bool LoadAsXML
        {
            get
            {
                return Convert.ToBoolean(ConfigurationManager.AppSettings["LoadAsXML"]);
            }
        }

        public static bool UseEncryption
        {
            get
            {
                return Convert.ToBoolean(ConfigurationManager.AppSettings["UseEncryption"]);
            }
        }

        public static bool AutoAccept
        {
            get
            {
                return Convert.ToBoolean(ConfigurationManager.AppSettings["AutoAccept"]);
            }
        }

        public static bool CreateAsyncRecord
        {
            get
            {
                return Convert.ToBoolean(ConfigurationManager.AppSettings["CreateAsyncRecord"]);
            }
        }

        public static string AsyncURL
        {
            get
            {
                return ConfigurationManager.AppSettings["AsyncURL"];
            }
        }

        public static bool SchemaValidationOn
        {
            get
            {
                return Convert.ToBoolean(ConfigurationManager.AppSettings["SchemaValidationOn"]);
            }
        }

        public static string SchemaLocation
        {
            get
            {
                return ConfigurationManager.AppSettings["SchemaLocation"];
            }
        }

        #endregion
    }
}

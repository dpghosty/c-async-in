﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.1008
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System.Net.Security;
using System.Runtime.Serialization;
using System;
using www.cjis.iowa.gov.WSResponse.one.zero;
using www.cjis.iowa.gov.CJISExchangeESBFault;

namespace ESBCurrent
{

    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace = "http://www.cjis.iowa.gov/WSResponse/1.0", ConfigurationName = "ESBCurrent.IowaAsyncPortType")]
    public interface IowaAsyncPortType
    {

        // CODEGEN: Generating message contract since the operation SubmitAsync is neither RPC nor document wrapped.
        [System.ServiceModel.OperationContractAttribute(Action = "http://www.cjis.iowa.gov/WSResponse/1.0/SubmitAsync", ReplyAction = "*")]
        [System.ServiceModel.FaultContractAttribute(typeof(FaultInfo), Action = "http://www.cjis.iowa.gov/WSResponse/1.0/SubmitAsync", Name = "CJISExchangeESBFault", Namespace = "http://www.cjis.iowa.gov/CJISExchangeESBFault", ProtectionLevel = ProtectionLevel.None)]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults = true)]
        MessageResponse SubmitAsync(MessageRequest request);
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped = false)]
    public partial class MessageRequest
    {

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://www.cjis.iowa.gov/WSResponse/1.0", Order = 0)]
        public ResponseMessageType WSResponseRequest;

        public MessageRequest()
        {
        }

        public MessageRequest(ResponseMessageType WSResponseRequest)
        {
            this.WSResponseRequest = WSResponseRequest;
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped = false)]
    public partial class MessageResponse
    {

        [System.ServiceModel.MessageBodyMemberAttribute(Namespace = "http://www.cjis.iowa.gov/WSResponse/1.0", Order = 0)]
        public ResponseMessageType WSResponse;

        public MessageResponse()
        {
        }

        public MessageResponse(ResponseMessageType WSResponse)
        {
            this.WSResponse = WSResponse;
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IowaAsyncPortTypeChannel : IowaAsyncPortType, System.ServiceModel.IClientChannel
    {
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class IowaAsyncPortTypeClient : System.ServiceModel.ClientBase<IowaAsyncPortType>, IowaAsyncPortType
    {

        public IowaAsyncPortTypeClient()
        {
        }

        public IowaAsyncPortTypeClient(string endpointConfigurationName) :
            base(endpointConfigurationName)
        {
        }

        public IowaAsyncPortTypeClient(string endpointConfigurationName, string remoteAddress) :
            base(endpointConfigurationName, remoteAddress)
        {
        }

        public IowaAsyncPortTypeClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) :
            base(endpointConfigurationName, remoteAddress)
        {
        }

        public IowaAsyncPortTypeClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) :
            base(binding, remoteAddress)
        {
        }

        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        MessageResponse IowaAsyncPortType.SubmitAsync(MessageRequest request)
        {
            return base.Channel.SubmitAsync(request);
        }

        public ResponseMessageType SubmitAsync(ResponseMessageType WSResponseRequest)
        {
            MessageRequest inValue = new MessageRequest();
            inValue.WSResponseRequest = WSResponseRequest;
            MessageResponse retVal = ((IowaAsyncPortType)(this)).SubmitAsync(inValue);
            return retVal.WSResponse;
        }
    }
}

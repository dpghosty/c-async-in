using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Services;
using System.Web.Services.Protocols;

namespace www.cjis.iowa.gov.WSResponse.one.zero
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.1015")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.cjis.iowa.gov/CJISExchangeHeader")]
    public partial class CJISExchangeHeaderType : object, System.ComponentModel.INotifyPropertyChanged
    {
        private string messageIDField;

        private string fromField;

        private string[] toField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MessageID
        {
            get
            {
                return this.messageIDField;
            }
            set
            {
                this.messageIDField = value;
                this.RaisePropertyChanged("MessageID");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string From
        {
            get
            {
                return this.fromField;
            }
            set
            {
                this.fromField = value;
                this.RaisePropertyChanged("From");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("To", Order = 2)]
        public string[] To
        {
            get
            {
                return this.toField;
            }
            set
            {
                this.toField = value;
                this.RaisePropertyChanged("To");
            }
        }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ServiceModel;
using System.Runtime.Serialization;

namespace www.cjis.iowa.gov.CJISExchangeESBFault
{
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="FaultInfo", Namespace="http://www.cjis.iowa.gov/CJISExchangeESBFault")]
    [System.SerializableAttribute()]
    public partial class FaultInfo : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        private string FaultCodeField;
        
        private string FaultMessageField;
        
        private string FaultDetailField;

        public FaultInfo(string vFaultCode, string vFaultMessage, string vfaultDetail)
        {
            FaultCodeField = vFaultCode;
            FaultMessageField = vFaultMessage;
            FaultDetailField = vfaultDetail;
        }

        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, EmitDefaultValue=false)]
        public string FaultCode {
            get {
                return this.FaultCodeField;
            }
            set {
                if ((object.ReferenceEquals(this.FaultCodeField, value) != true)) {
                    this.FaultCodeField = value;
                    this.RaisePropertyChanged("FaultCode");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, EmitDefaultValue=false)]
        public string FaultMessage {
            get {
                return this.FaultMessageField;
            }
            set {
                if ((object.ReferenceEquals(this.FaultMessageField, value) != true)) {
                    this.FaultMessageField = value;
                    this.RaisePropertyChanged("FaultMessage");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=2)]
        public string FaultDetail {
            get {
                return this.FaultDetailField;
            }
            set {
                if ((object.ReferenceEquals(this.FaultDetailField, value) != true)) {
                    this.FaultDetailField = value;
                    this.RaisePropertyChanged("FaultDetail");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
}

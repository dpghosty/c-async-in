using System;
using System.Collections.Generic;
using System.Text;

namespace www.cjis.iowa.gov.WSResponse.one.zero
{
    public enum ErrorItemType
    {
        System = 1,
        //
        User = 2,
        //
        Success = 9,
    }

    public enum EmailModeType
    {
        Always = 1,
        //
        Default = 2,
        //
        Never = 9,
    }
}

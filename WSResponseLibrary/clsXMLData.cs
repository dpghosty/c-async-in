﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace www.cjis.iowa.gov.WSResponse.one.zero
{
    public class clsXMLData
    {
        public static string SerializeObject(Object pObject)
        {
            string XmlizedString = string.Empty;

            MemoryStream memoryStream = new MemoryStream();
            XmlSerializer xs = new XmlSerializer(pObject.GetType());
            Encoding utf8EncodingWithNoByteOrderMark = new UTF8Encoding(false);
            XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, utf8EncodingWithNoByteOrderMark);

            xs.Serialize(xmlTextWriter, pObject);

            memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
            XmlizedString = UTF8ByteArrayToString(memoryStream.ToArray());

            return XmlizedString;
        }

        private static String UTF8ByteArrayToString(Byte[] characters)
        {
            UTF8Encoding encoding = new UTF8Encoding(false);
            String constructedString = encoding.GetString(characters);
            return (constructedString);
        }

        public static Object DeSerializeObject(string vXMLString, Type vType)
        {
            Object lObject = null;

            XmlSerializer xs = new XmlSerializer(vType);
            StringReader stringReader;
            stringReader = new StringReader(vXMLString);

            XmlTextReader xmlReader;
            xmlReader = new XmlTextReader(stringReader);

            lObject = xs.Deserialize(xmlReader);
            xmlReader.Close();
            stringReader.Close();

            return lObject;
        }
    }
}

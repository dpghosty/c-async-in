﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Net.Security;

using www.cjis.iowa.gov.WSResponse.one.zero;
using www.cjis.iowa.gov.CJISExchangeESBFault;

// NOTE: If you change the interface name "IService" here, you must also update the reference to "IService" in Web.config.
[ServiceContract(Name = "IowaAsyncPortType", Namespace = "http://www.cjis.iowa.gov/WSResponse/1.0")]
[XmlSerializerFormat(Style = OperationFormatStyle.Document, Use = OperationFormatUse.Literal)]
public interface IIowaAsyncInService
{
    //[OperationContract(Action = "", ReplyAction = "")]
    [OperationContract(Action="http://www.cjis.iowa.gov/WSResponse/1.0/SubmitAsync")]
    [FaultContractAttribute(typeof(FaultInfo), Action = "http://www.cjis.iowa.gov/CJISExchangeESBFault", Name = "CJISExchangeESBFault", Namespace = "http://www.cjis.iowa.gov/CJISExchangeESBFault", ProtectionLevel = ProtectionLevel.None)]
    MessageResponse SubmitAsync(MessageRequest info);

    // TODO: Add your service operations here
}

// Use a data contract as illustrated in the sample below to add composite types to service operations.
//[MessageContract (WrapperName = "MessageRequest", WrapperNamespace = "http://www.cjis.iowa.gov/ChargeTable/1.0")]
[MessageContract(IsWrapped = false)]
public class MessageRequest
{
    private System.Xml.XmlElement _MessageBody;

    [MessageBodyMember(Name = "WSResponseRequest", Namespace = "http://www.cjis.iowa.gov/WSResponse/1.0")]
    [System.Xml.Serialization.XmlAnyElementAttribute()]
    public System.Xml.XmlElement MessageBody
    {
        get
        {
            return _MessageBody;
        }
        set
        {
            _MessageBody = value;
        }
    }
}

//[MessageContract(WrapperName = "MessageResponse", WrapperNamespace = "http://www.cjis.iowa.gov/WSResponse/1.0")]
[MessageContract(IsWrapped = false)]
public class MessageResponse
{
    private ResponseMessageType _Response;

    [MessageBodyMember(Name = "WSResponse", Namespace = "http://www.cjis.iowa.gov/WSResponse/1.0")]
    public ResponseMessageType Response
    {
        get
        {
            return _Response;
        }
        set
        {
            _Response = value;
        }
    }
}


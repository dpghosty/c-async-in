﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Configuration;
using System.Xml;
using System.Web;
using System.Diagnostics;
using System.Web.Services.Protocols;

using www.cjis.iowa.gov.WSResponse.one.zero;

// NOTE: If you change the class name "Service" here, you must also update the reference to "Service" in Web.config and in the associated .svc file.
[ServiceBehavior(Name = "IowaAsyncService", Namespace = "http://www.cjis.iowa.gov/WSResponse/1.0")]
public class IowaAsyncInService : IIowaAsyncInService
{
    #region IIowaAsyncInService Members

    public MessageResponse SubmitAsync(MessageRequest info)
    {

        ResponseMessageType request = null;
        ResponseMessageType response = null;
        string lstrErrorMsg = string.Empty;
        string lstrBody = null;


        if (info == null)
        {
            lstrErrorMsg += "MessageRequest cannot be null.\n";
        }
        else
        {
            if (info.MessageBody == null)
            {
                lstrErrorMsg += "Root element cannot be null.\n";
            }
        }

        if (lstrErrorMsg == string.Empty)
        { // pass Business Validation, try schema validation

            lstrBody = info.MessageBody.OuterXml;

            request = new ResponseMessageType();
            request.RawPayload = lstrBody;

            if (request.SchemaValidation(true) == string.Empty)
            {
                request = ResponseMessageType.GetInstanceFromXML(lstrBody);
            }

            if (request != null)
            {
                request.RawPayload = lstrBody;
            }

            request.SentStatus = Globals.EndMessageStatus;

            request.IsOutGoing = false;
            request.OriginalMessageDirection = Globals.c_gstrMessageDirectionOut;
            request.SentStatus = Globals.EndMessageStatus;

            response = request.save(Globals.c_gstrActionCodeTypeAdd, true);
        } // end of successful Business Validation
        else
        {
            if (request == null)
            {
                lstrErrorMsg = "MessageID: UNKNOWN \n" + lstrErrorMsg + "\n";
            }
            else
            {
                lstrErrorMsg = request.ErrorHeader + " \n" + lstrErrorMsg + "\n";
            }

            lstrErrorMsg += "Occurred At: " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\n";

            ErrorItem.ThrowSoapException("Validation Error", SoapException.ClientFaultCode, lstrErrorMsg);
        }

        // when response is not a success, throw soap exception
        if (response == null)
        {
            if (request == null)
            {
                lstrErrorMsg = "MessageID: UNKNOWN \n" + lstrErrorMsg + "\n";
            }
            else
            {
                lstrErrorMsg = request.ErrorHeader + " \n" + lstrErrorMsg + "\n";
            }

            lstrErrorMsg += "Error message is not available." + "\n";
            lstrErrorMsg += "Occurred At: " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\n";

            ErrorItem.ThrowSoapException("Submission error", SoapException.ServerFaultCode, lstrErrorMsg);
        }
        else
        {
            if (response.TransactionStatus == Globals.c_gstrTransactionStatusFailure)
            {
                if (request == null)
                {
                    lstrErrorMsg = "MessageID: UNKNOWN \n" + lstrErrorMsg + "\n";
                }
                else
                {
                    lstrErrorMsg = request.ErrorHeader + " \n" + lstrErrorMsg + "\n";
                }

                lstrErrorMsg += response.ErrorMsg + "\n";
                lstrErrorMsg += "Occurred At: " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\n";

                ErrorItem.ThrowSoapException("Submission error", SoapException.ServerFaultCode, lstrErrorMsg);
            }

            if (response.TransactionStatus == Globals.c_gstrTransactionStatusSuccess)
            {
                string sb = string.Empty;
                sb += "MessageID: " + response.MessageId + "\n";
                sb += response.ErrorMsg + "\n";

                ErrorItem.WriteEventLog(sb, EventLogEntryType.Information, ErrorItemType.User, EmailModeType.Default);
            }
        }
                
        MessageResponse mr = new MessageResponse();
        mr.Response = response;

        return mr;
    }

    #endregion
}
